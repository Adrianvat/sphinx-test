﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SPHINX.Models;

namespace SPHINX.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ManufacturersController : ControllerBase
    {
        private readonly IRepository<Manufacturer> _manufacturerRepository;

        public ManufacturersController(IRepository<Manufacturer> manufacturerRepository)
        {
            _manufacturerRepository = manufacturerRepository;
        }
        // GET: api/Manufacturers
        [HttpGet]
        public IEnumerable<Manufacturer> Get()
        {
            return _manufacturerRepository.GetAll().ToList();
        }

        // GET: api/Manufacturers/5
        [HttpGet("{id}")]
        public Manufacturer Get(int id)
        {
            return _manufacturerRepository.GetById(id);
        }

        // POST: api/Manufacturers
        [HttpPost]
        public void Post([FromBody] Manufacturer value)
        {
            _manufacturerRepository.Create(value);
        }

        // PUT: api/Manufacturers/5
        [HttpPut]
        public void Put([FromBody] Manufacturer value)
        {
            _manufacturerRepository.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _manufacturerRepository.Delete(_manufacturerRepository.GetById(id));
        }
    }
}
