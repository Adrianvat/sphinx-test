﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SPHINX.Models;

namespace SPHINX.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarTypesController : ControllerBase
    {
        private readonly IRepository<CarType> _carTypeRepository;

        public CarTypesController(IRepository<CarType> repository)
        {
            _carTypeRepository = repository;
        }
        // GET: api/CarTypes
        [HttpGet]
        public IEnumerable<CarType> Get()
        {
            return _carTypeRepository.GetAll().ToList();
        }

        // GET: api/CarTypes/5
        [HttpGet("{id}")]
        public CarType Get(int id)
        {
            return _carTypeRepository.GetById(id); ;
        }

        // POST: api/CarTypes
        [HttpPost]
        public void Post([FromBody] CarType value)
        {
            _carTypeRepository.Create(value);
        }

        // PUT: api/CarTypes/5
        [HttpPut("{id}")]
        public void Put([FromBody] CarType value)
        {
            _carTypeRepository.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _carTypeRepository.Delete(_carTypeRepository.GetById(id));
        }
    }
}
