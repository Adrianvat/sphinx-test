﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SPHINX.Models;

namespace SPHINX.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        private readonly IRepository<Car> _carRepository;
        public CarsController(IRepository<Car> CarRepository)
        {
            _carRepository = CarRepository;
        }
        // GET: api/Cars
        [HttpGet]
        public IEnumerable<Car> Get()
        {
            return _carRepository.GetAll().ToList();
        }

        // GET: api/Cars/5
        [HttpGet("{id}")]
        public Car Get(int id)
        {
            return _carRepository.GetById(id);
        }

        // POST: api/Cars
        [HttpPost]
        public void Post([FromBody] Car value)
        {
            _carRepository.Create(value);
        }

        // PUT: api/Cars/5
        [HttpPut]
        public void Put([FromBody] Car value)
        {
            _carRepository.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var car = _carRepository.GetById(id);
            _carRepository.Delete(car);
        }
    }
}
