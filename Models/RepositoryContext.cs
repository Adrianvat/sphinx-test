﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPHINX.Models
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options) 
            :base(options)
        {
        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<CarType> CarTypes { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
    }
}
