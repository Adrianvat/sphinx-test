﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPHINX.Models
{
    public class CarTypeRepository : IRepository<CarType>
    {
        private readonly RepositoryContext _repositoryContext;
        public CarTypeRepository(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public void Create(CarType entity)
        {
            _repositoryContext.CarTypes.Add(entity);
            _repositoryContext.SaveChanges();
        }

        public void Delete(CarType entity)
        {
            _repositoryContext.CarTypes.Remove(entity);
            _repositoryContext.SaveChanges();
        }

        public IQueryable<CarType> GetAll()
        {
            return _repositoryContext.CarTypes;
        }

        public CarType GetById(int id)
        {
            return _repositoryContext.CarTypes.FirstOrDefault(ct => ct.Id == id);
        }

        public void Update(CarType entity)
        {
            var oldCarType = _repositoryContext.CarTypes.Find(entity.Id);
            _repositoryContext.Entry(oldCarType).CurrentValues.SetValues(entity);
            _repositoryContext.SaveChanges();
        }
    }
}
