﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SPHINX.Models
{
    [Table("CarTypes")]
    public class CarType
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
