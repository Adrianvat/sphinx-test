﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPHINX.Models
{
    public class CarRepository : IRepository<Car>
    {
        private readonly RepositoryContext _repositoryContext;

        public CarRepository(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }
        public void Create(Car entity)
        {
            _repositoryContext.Cars.Add(entity);
            _repositoryContext.SaveChanges();
        }

        public void Delete(Car entity)
        {
            _repositoryContext.Cars.Remove(entity);
            _repositoryContext.SaveChanges();
        }

        public IQueryable<Car> GetAll()
        {
            return _repositoryContext.Cars;
        }

        public Car GetById(int id)
        {
            return _repositoryContext.Cars.FirstOrDefault(car => car.Id == id);
        }

        public void Update(Car entity)
        {
            var oldCar = _repositoryContext.Cars.FirstOrDefault(c => c.Id == entity.Id);
            _repositoryContext.Entry(oldCar).CurrentValues.SetValues(entity);
            _repositoryContext.SaveChanges();
        }
    }
}
