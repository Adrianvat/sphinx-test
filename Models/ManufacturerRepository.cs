﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPHINX.Models
{
    public class ManufacturerRepository : IRepository<Manufacturer>
    {
        private readonly RepositoryContext _repositoryContext;
        public ManufacturerRepository(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        public void Create(Manufacturer entity)
        {
            _repositoryContext.Manufacturers.Add(entity);
            _repositoryContext.SaveChanges();
        }

        public void Delete(Manufacturer entity)
        {
            _repositoryContext.Manufacturers.Remove(entity);
            _repositoryContext.SaveChanges();
        }

        public IQueryable<Manufacturer> GetAll()
        {
            return _repositoryContext.Manufacturers;
        }

        public Manufacturer GetById(int id)
        {
            return _repositoryContext.Manufacturers.FirstOrDefault(m => m.Id == id);
        }

        public void Update(Manufacturer entity)
        {
            var oldManufacturer = _repositoryContext.Manufacturers.Find(entity.Id);
            _repositoryContext.Entry(oldManufacturer).CurrentValues.SetValues(entity);
            _repositoryContext.SaveChanges();
        }
    }
}
