﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SPHINX.Models
{
    [Table("Cars")]
    public class Car
    {
      
        [Key]
        public int Id { get; set; }
        public string ModelName { get; set; }
        public int ManufacturerId { get; set; }
        public int CubicCapacity { get; set; }
        public int HorsePower { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int CarTypeId { get; set; }

    }
}
